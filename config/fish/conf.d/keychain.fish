if status --is-interactive
	keychain --agents "ssh" --eval --quiet -Q id_ed_codeberg | source
	# nothing to source here; and i'm not running gpg-agent for ssh mode, because i don't trust it
	keychain --quiet --agents gpg
end
