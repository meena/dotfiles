if which pyenv
	set -xU PYENV_ROOT "$HOME/.pyenv"
	set -U fish_user_paths "$PYENV_ROOT/bin" $fish_user_paths
	pyenv init - | source

	register-python-argcomplete --shell fish pipx | source
end
