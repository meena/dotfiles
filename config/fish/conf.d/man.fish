if status --is-interactive
    # solaris spoiled me…
    set -xU MANWIDTH 72

    set -xU MANPAGER 'nvim +Man!'
end
